## before entering bellow command install node js

## to install react tools to create project
npm install -g create-react-app

## to create a new project
create-react-app project1


## to enable routing install
npm install --save react-router-dom

## import routing library in App.js
import {BrowserRouter as Router, HashRouter, Switch, Route} from "react-router-dom"

## to install sass alternative to css
npm install node-sass@4.14.1 --save

## if u cloning code from git in any other computer,the node module will not present,u can reinstall all module back by bellow command
npm i


## to install bootstrap4
## install jquery first
<!-- https://react-bootstrap.github.io/getting-started/introduction/ -->
npm i jquery --save

##  then install bootstrap
npm install react-bootstrap bootstrap --save



