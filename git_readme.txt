1. To insitalise project first time
git init


2. If u change any file
git add changed_file_name

or

if u don't know actually what are the files modified(to add all  modified file)
git add .


3.  need to commit modified file (-m means some message or comment to identify that commit)
git commit -m "message/comment"


4. If u first time created project u need to link repo to the repo created in git server

git remote add origin git_url

to change the url (incase u typed wrong url)
git remote set-url origin  git_url


5. to update changes to git server
git push origin master


6. to get  changes from git server to local computer
git pull origin master

7. to clone all code again in different directory or computer
git clone git_url

